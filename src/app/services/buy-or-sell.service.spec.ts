import { TestBed } from '@angular/core/testing';

import { BuyOrSellService } from './buy-or-sell.service';

describe('BuyOrSellService', () => {
  let service: BuyOrSellService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BuyOrSellService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
