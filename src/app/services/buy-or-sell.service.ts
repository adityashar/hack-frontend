import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { UserInput } from '../common/user-input';
import { Holdings } from '../common/holdings';
import { OrderStock } from '../common/order-stock';
import { User } from '../common/user';

@Injectable({
  providedIn: 'root'
})
export class BuyOrSellService {

  private baseUrl ='http://localhost:8080/';

  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  } 

  buyOrder(order: UserInput): Observable<object> {

    let data = {
      n: order.volumne,
      companyname: order.stockName,
      choice: order.type,
      p: order.price
      
    }

    return this.httpClient.get<object>(this.baseUrl + `buy?n=${data.n}&companyname=${data.companyname}&choice=${data.choice}&p=${data.p}` )
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  sellOrder(data: UserInput): Observable<object> {
    return this.httpClient.get<object>(this.baseUrl + `sell?numofstocks=${data.volumne}&companyname=${data.stockName}&choice=${data.type}&stoploss=${data.price}`)
    .pipe (
      retry(1),
      catchError(this.handleError)
    )
  }

  holdingsData(): Observable<Holdings[]> {
    return this.httpClient.get<Holdings[]>(this.baseUrl + 'holding')
    .pipe (
      retry(1),
      catchError(this.handleError)
    )
  }

  ordersData(): Observable<OrderStock[]> {
    return this.httpClient.get<OrderStock[]>(this.baseUrl + 'orders')
    .pipe (
      retry(1),
      catchError(this.handleError)
    )
  }

  findUser(user: User): Observable<string> {
    return this.httpClient.get<string>(this.baseUrl + `login?useremail=${user.useremail}&password=${user.password}`)
    .pipe (
      retry(1),
      catchError(this.handleError)
    )
  }

  registerUser(user: User): Observable<number> {
    return this.httpClient.get<number>(this.baseUrl + `createuser?useremail=${user.useremail}&password=${user.password}&firstname=${user.firstname}&lastname=${user.lastname}`)
    .pipe (
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }

}
