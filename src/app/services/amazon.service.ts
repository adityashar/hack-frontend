import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stock } from '../common/Stock';
import { Amazon } from '../common/amazon';

@Injectable({
  providedIn: 'root'
})
export class AmazonService {

  private baseUrl ='http://localhost:8080/amazons';

  constructor(private httpClient: HttpClient) { }

  getAmazonlist(): Observable<Amazon[]>{
    return this.httpClient.get<GetResponse>(this.baseUrl).pipe(
      map(response => response._embedded.amazons)
    );
  }
}
  
interface GetResponse{
  _embedded:{
    amazons: Amazon[];
  }
}

