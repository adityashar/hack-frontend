import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stock } from '../common/Stock';
@Injectable({
  providedIn: 'root'
})
export class StockService {

  private baseUrl ='http://localhost:8080/stock';

  constructor(private httpClient: HttpClient) { }

  getStocklist(): Observable<Stock[]>{
    return this.httpClient.get<GetResponse>(this.baseUrl).pipe(
      map(response => response._embedded.stock)
    );
  }
}
  
interface GetResponse{
  _embedded:{
    stock: Stock[];
  }
}