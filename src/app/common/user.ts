export class User {
    useremail: string;
    password: string;
    firstname: string;
    lastname: string;

    constructor() {
        this.firstname = "";
        this.password = "";
        this.useremail = "";
        this.lastname = "";
    }
}
