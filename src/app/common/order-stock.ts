export class OrderStock {
    orderId: number;
    companyId: string;
    numberOfStocks: number;
    stockPrice: number;
    totalCost: number;
    buyorsell: string;
    statuscode: number;
    companyName: string

    constructor() {
        this.companyId="";
        this.numberOfStocks=0;
        this.orderId=0;
        this.stockPrice=0;
        this.totalCost=0;
        this.buyorsell='';
        this.statuscode=0;
        this.companyName="";
    }
}
