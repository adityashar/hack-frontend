export class UserInput {

    public stockName: string;
    public volumne: number;
    public type: 'MARKET' | 'LIMIT';        
    public price: number;
    
    constructor() {
        this.stockName = '';
        this.price = 0.0;
        this.type = 'MARKET';
        this.volumne = 0;
    }

}
