export class Holdings {
    holdingId: number;
    companyName: string;
    numberOfStocks: number;
    buyingprice: number;
    total: number;

    constructor() {
        this.numberOfStocks = 0;
        this.buyingprice = 0.0;
        this.companyName = "";
        this.holdingId = 0;
        this.total = 0.0;
    }
}
