export class Amazon {
    companyId!: Int32Array;
    companyname!: string;
    transaction_datetime!: Date;
    low_value!: Float32Array;
    open_value!: Float32Array;
    volume!: Float32Array;
    high_value!: Float32Array;
    close_value!: Float32Array;
    adjustedclose!: Float32Array;
}
