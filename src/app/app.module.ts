import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { StockListComponent } from './components/stock-list/stock-list.component';
import { HttpClientModule } from '@angular/common/http';
import { StockService } from './services/Stock.service';
import { AmazonListComponent } from './components/amazon-list/amazon-list.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { InputFormComponent } from './components/input-form/input-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SellFormComponent } from './components/sell-form/sell-form.component'; 

//import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { OrderComponent } from './components/order/order.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';


const routes: Routes = [
  { path: 'stocks', component: StockListComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'order', component: OrderComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    StockListComponent,
    AmazonListComponent,
    DashboardComponent,
    InputFormComponent,
    SellFormComponent,
    OrderComponent,
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MaterialModule,
    NgbModule,
    FormsModule,
  ],
  providers: [StockService],
  bootstrap: [AppComponent]
})
export class AppModule { }
