import { trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Stock } from 'src/app/common/Stock';
import { UserInput } from 'src/app/common/user-input';
import { BuyOrSellService } from 'src/app/services/buy-or-sell.service';
import { StockService } from 'src/app/services/Stock.service';

@Component({
  selector: 'app-sell-form',
  templateUrl: './sell-form.component.html',
  styleUrls: ['./sell-form.component.css']
})
export class SellFormComponent implements OnInit {

  closeModal: string;
  stocks: Stock[];

  userData: UserInput;

  constructor(private modalService: NgbModal, private stockservice: StockService, private buyorsellService: BuyOrSellService) {
    this.closeModal = '';
    this.stocks = [];
    this.userData = new UserInput();
  }

  ngOnInit() {
    this.getStocks();
  }

  getStocks() {
    this.stockservice.getStocklist().subscribe(
      data => {
        this.stocks = data;
      }
    )
  }
    
  triggerModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }
  
  onSubmit() {
    console.log(this.userData);
    this.buyorsellService.sellOrder(this.userData).subscribe(res => {
      console.log(res);
    })
  }

  public componentMethod( event: any ) { event.target.select(); }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
