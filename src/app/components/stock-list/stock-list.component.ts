import { Component, OnInit } from '@angular/core';
import { Stock } from 'src/app/common/Stock';
import { StockService } from 'src/app/services/Stock.service';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent implements OnInit {

  stock: Stock[] | undefined ;

  constructor(private stockservice: StockService) { }

  ngOnInit(){
    this.liststock();
  }
  liststock(){
    this.stockservice.getStocklist().subscribe(
      data => {
        this.stock=data;
      }
    )
  }

}
