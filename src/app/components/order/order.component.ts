import { Component, OnInit } from '@angular/core';
import { OrderStock } from 'src/app/common/order-stock';
import { BuyOrSellService } from 'src/app/services/buy-or-sell.service';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  displayedColumns: string[];
  dataSource: OrderStock[];

  constructor(private orderservice: BuyOrSellService) {
    this.dataSource = [];
    this.displayedColumns = ['orderId', 'companyName', 'numberOfStocks', 'stockPrice', 'totalCost', 'buyorsell', 'statuscode'];
  }

  ngOnInit(): void {
    this.orderservice.ordersData().subscribe(data => {
       this.dataSource=data;
       console.log(data);
     })
  }

}
