import { Component, OnInit } from '@angular/core';
import { Holdings } from 'src/app/common/holdings';
import { BuyOrSellService } from 'src/app/services/buy-or-sell.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  displayedColumns: string[];
  dataSource: Holdings[];
  
  constructor(private holdingservice: BuyOrSellService) {
    this.dataSource = [];
    this.displayedColumns = ['holdingId', 'companyName', 'numberOfStocks', 'buyingprice', 'total'];
  }

  ngOnInit(): void {
     this.holdingservice.holdingsData().subscribe(data => {
       console.log(data[0].companyName);
       this.dataSource=data;
     })
  }

}
