import { Component } from '@angular/core';
import { User } from 'src/app/common/user';
import { BuyOrSellService } from 'src/app/services/buy-or-sell.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user: User;

  constructor(private userService: BuyOrSellService, private route: Router) {
    this.user = new User();
  }

  onSubmit(event: any) {
    event.preventDefault();

    this.userService.registerUser(this.user).subscribe(res => {
      console.log(res);
       if(res === 1) {
         this.route.navigate(['/login']);
       }
       else {
         alert('Registration failed!');
       }
     })
  }

}
