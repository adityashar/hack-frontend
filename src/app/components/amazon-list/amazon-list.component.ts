import { Component, OnInit } from '@angular/core';
import { Amazon } from 'src/app/common/amazon';
import { AmazonService } from 'src/app/services/amazon.service';

@Component({
  selector: 'app-amazon-list',
  templateUrl: './amazon-list.component.html',
  styleUrls: ['./amazon-list.component.css']
})
export class AmazonListComponent implements OnInit {

  amazon: Amazon[] | undefined;

  constructor(private amazonservice: AmazonService) { }

  ngOnInit(){
    this.listamazon();
  }
  listamazon(){
    this.amazonservice.getAmazonlist().subscribe(
      data => {
        this.amazon=data;
      }
    )
    }
}
