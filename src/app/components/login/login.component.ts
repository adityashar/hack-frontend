import { Component } from '@angular/core';
import { User } from 'src/app/common/user';
import { BuyOrSellService } from 'src/app/services/buy-or-sell.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  user: User;

  constructor(private userService: BuyOrSellService, private route: Router) {
    this.user = new User();
  }

  onSubmit(event: any) {
    event.preventDefault();
    // this.route.navigate(['/stocks']);
    this.userService.findUser(this.user).subscribe(res => {
      if(res !== "") {
        this.user.firstname = res;
        this.route.navigate(['/stocks']);
      }
      else {
        alert('Login failed!');
      }
    })
  }

}
