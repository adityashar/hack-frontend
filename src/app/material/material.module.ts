import { NgModule } from '@angular/core';

import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  imports: [
    MatButtonModule,
    MatTableModule,
    MatCardModule
  ],
  exports: [
    MatButtonModule,
    MatTableModule,
    MatCardModule
  ]
})
export class MaterialModule { }
